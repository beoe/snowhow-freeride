cordova.define("info.snowhow.plugin.unzip.Unzip", function(require, exports, module) {
/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
*/

var exec = require('cordova/exec');

/**
 * Unzip a file to sdcard
 */

var Unzip = function() {
  this.onprogress = null;
};


/**
 * unzip a file
 *
 * @param {String} src_file       Source zip file
 * @param {String} dst_path       Destination path
 */
Unzip.prototype.unzip = function(src_file, dst_path, succ, err) {
  var self = this;
  var win = function(result) {
    if (result.progress) {
      if (self.onprogress) {
        self.onprogress(result);
      }
    } else if (succ) {
      succ(result);
    }
  };
  exec(win, err, "Unzip", "unzip", [src_file, dst_path]);
};


module.exports = Unzip;


});
